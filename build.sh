#!/usr/bin/env bash

set -euo pipefail

eval "$(/opt/homebrew/bin/brew shellenv)"

export RUSTUP_TOOLCHAIN=1.52.1
export LDFLAGS='-L/opt/homebrew/opt/libiconv/lib -L/opt/homebrew/opt/zlib/lib -L/opt/homebrew/lib'
export CPPFLAGS='-I/opt/homebrew/opt/libiconv/include -I/opt/homebrew/opt/zlib/include'
export PKG_CONFIG_PATH="/opt/homebrew/opt/zlib/lib/pkgconfig"
export LIBRARY_PATH=/opt/homebrew/lib
export OPAMASSUMEDEPEXTS=1
export TMPDIR=~/tmp/

set -x

# rustup toolchain install $RUSTUP_TOOLCHAIN --profile minimal
rustup-init --profile minimal --default-toolchain $RUSTUP_TOOLCHAIN -y

source $HOME/.cargo/env

cd ext/tezos
make build-deps
eval $(opam env)
dune build src/bin_client/main_client.exe
dune build src/bin_client/main_admin.exe
dune build src/bin_node/main.exe
cd -

mkdir -p build
mv -f ext/tezos/_build/default/src/bin_client/main_client.exe  build/tezos-client
mv -f ext/tezos/_build/default/src/bin_client/main_admin.exe   build/tezos-admin-client
mv -f ext/tezos/_build/default/src/bin_node/main.exe           build/tezos-node
