main:
	./build.sh

clean:
	rm -rf _opam build bin ext/tezos/{_opam,_build}
	cd ext/tezos; make clean

.PHONY: main clean

